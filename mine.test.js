const checkPairsCombs = require('./utils/combinations/checkPairsCombs/checkPair');
const checkStraightFlush = require('./utils/combinations/checkFlushCombs/checkStraightFlush');
const checkThreeCombs = require('./utils/combinations/checkPairsCombs/checkThreeKind');
const checkFourKindCombs = require('./utils/combinations/checkPairsCombs/checkFourKind');
const checkFullHouse = require('./utils/combinations/checkPairsCombs/checkFullHouse');
const checkTwoPairsCombs = require('./utils/combinations/checkPairsCombs/checkTwoPairs');
const checkStraight = require('./utils/combinations/checkFlushCombs/checkStraight');
const checkFlush = require('./utils/combinations/checkFlushCombs/checkFlush');
const checkRoyalFlush = require('./utils/combinations/checkFlushCombs/checkRoyalFlush');
const senComb = require('./utils/seniorCombination');
const checkHighCardCombs = require('./utils/combinations/checkPairsCombs/checkHighCard');
const checkCorrectData = require('./utils/combinations/checkPairsCombs/checkMinMaxLenght')
describe('Check all combinations', () => {
  const undefinedSetup = [];
  const highCardSetup = [
    {value: 12, suit: '♥'},
    {value: 5, suit: '♦'},
    {value: 2, suit: '♥'},
    {value: 10, suit: '♦'},
    {value: 7, suit: '♥'},
    {value: 6, suit: '♦'},
    {value: 3, suit: '♥'},
  ];
  const pairSetup = [
    {value: 14, suit: '♥'},
    {value: 14, suit: '♦'},
    {value: 7, suit: '♥'},
    {value: 12, suit: '♦'},
    {value: 5, suit: '♥'},
    {value: 10, suit: '♦'},
    {value: 3, suit: '♥'},
  ];
  const twoPairSetup = [
    {value: 13, suit: '♥'},
    {value: 13, suit: '♠'},
    {value: 12, suit: '♦'},
    {value: 12, suit: '♠'},
    {value: 10, suit: '♥'},
    {value: 9, suit: '♥'},
    {value: 3, suit: '♥'},
  ];
  const threeSetup = [
    {value: 13, suit: '♣'},
    {value: 13, suit: '♠'},
    {value: 13, suit: '♦'},
    {value: 12, suit: '♣'},
    {value: 11, suit: '♦'},
    {value: 9, suit: '♣'},
    {value: 3, suit: '♣'},
  ];
  const straightSetup = [
    {value: 14, suit: '♥'},
    {value: 13, suit: '♠'},
    {value: 12, suit: '♦'},
    {value: 11, suit: '♠'},
    {value: 10, suit: '♥'},
    {value: 9, suit: '♥'},
    {value: 3, suit: '♥'},
  ];
  const flushSetup = [
    {value: 13, suit: '♣'},
    {value: 13, suit: '♠'},
    {value: 12, suit: '♦'},
    {value: 11, suit: '♣'},
    {value: 11, suit: '♣'},
    {value: 9, suit: '♣'},
    {value: 3, suit: '♣'},
  ];
  const fourSetup = [
    {value: 12, suit: '♥'},
    {value: 12, suit: '♣'},
    {value: 12, suit: '♦'},
    {value: 12, suit: '♠'},
    {value: 8, suit: '♥'},
    {value: 4, suit: '♥'},
    {value: 3, suit: '♥'},
  ];
  const fullHouseSetup = [
    {value: 13, suit: '♣'},
    {value: 13, suit: '♠'},
    {value: 13, suit: '♦'},
    {value: 11, suit: '♠'},
    {value: 11, suit: '♦'},
    {value: 9, suit: '♠'},
    {value: 3, suit: '♥'},
  ];
  const straightFlushSetup = [
    {value: 13, suit: '♥'},
    {value: 13, suit: '♠'},
    {value: 12, suit: '♠'},
    {value: 11, suit: '♠'},
    {value: 10, suit: '♠'},
    {value: 9, suit: '♠'},
    {value: 3, suit: '♥'},
  ];
  const royalFlushSetup = [
    {value: 14, suit: '♠'},
    {value: 13, suit: '♠'},
    {value: 12, suit: '♠'},
    {value: 11, suit: '♠'},
    {value: 9, suit: '♠'},
    {value: 9, suit: '♥'},
    {value: 10, suit: '♠'},
  ];

  test('Royal Flush', () => {
    expect(checkRoyalFlush(royalFlushSetup)).toEqual(`Royal Flush`);
    expect(checkRoyalFlush(straightFlushSetup)).toEqual(undefined);
  });

  test('Flush', () => {
    expect(checkFlush(royalFlushSetup)).toEqual(`Flush`);
    expect(checkFlush(flushSetup)).toEqual(`Flush`);
    expect(checkFlush(fourSetup)).toEqual(undefined);
  });

  test('Straight', () => {
    expect(checkStraight(straightSetup)).toEqual(`Straight`);
    expect(checkStraight(straightFlushSetup)).toEqual(`Straight`);
    expect(checkStraight(flushSetup)).toEqual(undefined);
  });

  test('Two Pair', () => {
    expect(checkTwoPairsCombs(twoPairSetup)).toEqual(`Two Pair`);
    expect(checkTwoPairsCombs(pairSetup)).toEqual(undefined);
  });

  test('Full House', () => {
    expect(checkFullHouse(fullHouseSetup)).toEqual(`Full House`);
    expect(checkFullHouse(twoPairSetup)).toEqual(undefined);
    expect(checkFullHouse(threeSetup)).toEqual(undefined);
  });

  test('Four of a Kind', () => {
    expect(checkFourKindCombs(fourSetup)).toEqual(`Four of a Kind`);
    expect(checkFourKindCombs(fullHouseSetup)).toEqual(undefined);
  });

  test('Straight-flush', () => {
    expect(checkStraightFlush(straightFlushSetup)).toEqual(`Straight-flush`);
    expect(checkStraightFlush(royalFlushSetup)).toEqual(`Straight-flush`);
    expect(checkStraightFlush(straightSetup)).toEqual(undefined);
  });

  test('Three of a Kind', () => {
    expect(checkThreeCombs(fourSetup)).toEqual(`Three of a Kind`);
    expect(checkThreeCombs(threeSetup)).toEqual(`Three of a Kind`);
    expect(checkThreeCombs(pairSetup)).toEqual(undefined);
  });

  test('Pair', () => {
    expect(checkPairsCombs(twoPairSetup)).toEqual(`One Pair`);
    expect(checkPairsCombs(highCardSetup)).toEqual(undefined);
  });

  test('High card', () => {
    expect(checkHighCardCombs(highCardSetup)).toEqual(`High Card`);
    expect(checkHighCardCombs(undefinedSetup)).toEqual(undefined);
  });

  test('Correct Data', () => {
    expect(checkCorrectData(undefinedSetup)).toEqual(`Select 5-7 cards`);
    expect(checkCorrectData(highCardSetup)).toEqual(undefined);
  });

  test('Seniority', () => {
    expect(senComb(undefinedSetup)).toEqual('Select 5-7 cards');
    expect(senComb(highCardSetup)).toEqual('High Card');
    expect(senComb(pairSetup)).toEqual(`One Pair`);
    expect(senComb(twoPairSetup)).toEqual('Two Pair');
    expect(senComb(threeSetup)).toEqual('Three of a Kind');
    expect(senComb(straightSetup)).toEqual('Straight');
    expect(senComb(flushSetup)).toEqual('Flush');
    expect(senComb(fourSetup)).toEqual('Four of a Kind');
    expect(senComb(fullHouseSetup)).toEqual('Full House');
    expect(senComb(straightFlushSetup)).toEqual('Straight-flush');
    expect(senComb(royalFlushSetup)).toEqual('Royal Flush');
  });
});
