const DECK = require('../cardDeck');

function getCards(count) {
  const copyDeck = [...DECK];
  copyDeck.sort((a, b) => Math.random() - 0.5);
  const myDeck = [...copyDeck.slice(0, count)];
  return myDeck.sort((a, b) => b.value-a.value);
}
module.exports = getCards;
