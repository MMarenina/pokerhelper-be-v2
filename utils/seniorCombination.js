const checkStraightFlush = require('./combinations/checkFlushCombs/checkStraightFlush');
const checkFullHouse = require('./combinations/checkPairsCombs/checkFullHouse');
const checkStraight = require('./combinations/checkFlushCombs/checkStraight');
const checkFlush = require('./combinations/checkFlushCombs/checkFlush');
const checkRoyalFlush = require('./combinations/checkFlushCombs/checkRoyalFlush');
const checkFourKindCombs = require('./combinations/checkPairsCombs/checkFourKind');
const checkThreeCombs = require('./combinations/checkPairsCombs/checkThreeKind');
const checkTwoPairsCombs = require('./combinations/checkPairsCombs/checkTwoPairs');
const checkPairsCombs = require('./combinations/checkPairsCombs/checkPair');
const checkHighCardCombs = require('./combinations/checkPairsCombs/checkHighCard');
const checkCorrectData = require('./combinations/checkPairsCombs/checkMinMaxLenght');

function senComb(getCards) {
const combinationObj = {
  'Select 5-7 cards': checkCorrectData,
  'Royal Flush': checkRoyalFlush,
  'Straight-flush': checkStraightFlush,
  'Four of a Kind':checkFourKindCombs,
  'Full House': checkFullHouse,
  'Flush': checkFlush,
  'Straight': checkStraight,
  'Three of a Kind': checkThreeCombs,
  'Two Pair': checkTwoPairsCombs,
  'One Pair': checkPairsCombs,
  'High Card': checkHighCardCombs,
};

  let result = '';
  for (let key of Object.keys(combinationObj)) {
    if (combinationObj[key](getCards)) {
      result = key;
      return result;
    }
  }
}
module.exports = senComb;
