const transformCards = require('../transformCards');
function checkFullHouse(getCards) {
  const preparedCards = transformCards(getCards, 'value');
  if(preparedCards[0].count >= 3 && preparedCards[1].count >= 2){
    return `Full House`;
  }
}
module.exports = checkFullHouse;
