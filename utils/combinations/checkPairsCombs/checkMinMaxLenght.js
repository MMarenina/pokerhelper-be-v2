function checkCorrectData(cards) {
  const cardsMin = 5;
  const cardsMax = 7;
  if(cards.length < cardsMin || cards.length > cardsMax){
    return `Select 5-7 cards`;
  }
}
module.exports = checkCorrectData;
