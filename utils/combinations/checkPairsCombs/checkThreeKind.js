const transformCards = require('../transformCards');
function checkThreeCombs(getCards) {
  const preparedCards = transformCards(getCards, 'value');
  if(preparedCards[0].count >= 3){
    return `Three of a Kind`;
  }
}
module.exports = checkThreeCombs;
