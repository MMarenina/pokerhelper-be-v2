const transformCards = require('../transformCards');
function checkFourKindCombs(getCards) {
  const preparedCards = transformCards(getCards, 'value');
  if(preparedCards[0].count >= 4){
    return `Four of a Kind`;
  }
}
module.exports = checkFourKindCombs;
