const transformCards = require('../transformCards');
function checkTwoPairsCombs(getCards) {
  const preparedCards = transformCards(getCards, 'value');
   if(preparedCards[0].count >= 2 && preparedCards[1].count >= 2){
    return `Two Pair`;
  }
}
module.exports = checkTwoPairsCombs;
