const transformCards = require('../transformCards');
function checkPairsCombs(getCards) {
  const preparedCards = transformCards(getCards, 'value');
  if(preparedCards[0].count >= 2){
    return `One Pair`;
  }
}
module.exports = checkPairsCombs;
