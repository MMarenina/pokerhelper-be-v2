function transformCards(cards, prop) {
  const getCards = cards.sort((a, b) => b.value-a.value);
  const propertyValueArr = getCards.map(e => e[prop]);
  const propertySet = [...new Set(propertyValueArr)];
  const preparedArr =  propertySet.map(e => {
    return {
      [prop]: e,
      "count": propertyValueArr.filter(a => a === e).length
    };
  }).sort((a,b)=> b.count - a.count);
  return preparedArr;
}

module.exports = transformCards;
