const transformflush = require('../transformStritCards');
function checkRoyalFlush(getCards) {
  const flushCards = transformflush(getCards);
  if(flushCards.length !== 0) {
    if (flushCards[0].value === flushCards[4].value + 4 && flushCards[0].value === 14) {
      return `Royal Flush`;
    }
  }
}
module.exports = checkRoyalFlush;
