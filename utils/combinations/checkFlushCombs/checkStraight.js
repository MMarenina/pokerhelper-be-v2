function checkStraight(cards) {
  const getCards = cards.sort((a, b) => b.value-a.value);
  const propertyValueArr = getCards.map(e => e.value);
  const propertySet = [...new Set(propertyValueArr)];
  for (let i= 0; i < propertySet.length - 4; i++){
    if(propertySet[i] === propertySet[i+4]+4) {
       return `Straight`;
    }
  }
}
module.exports = checkStraight;
