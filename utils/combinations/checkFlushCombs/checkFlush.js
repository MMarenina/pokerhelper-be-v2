const transformCards = require('../transformCards');
function checkFlush(getCards) {
  const preparedCardsSuit = transformCards(getCards, 'suit');
  if(preparedCardsSuit[0].count >= 5){
    return `Flush`;
  }
}
module.exports = checkFlush;
