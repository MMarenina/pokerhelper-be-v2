const transformflush = require('../transformStritCards');
function checkStraightFlush(cards) {
  const flushCards = transformflush(cards);
  for (let i= 0; i< flushCards.length-4; i++){
    if(flushCards[i].value === flushCards[i+4].value+4){
      return 'Straight-flush';
    }
  }
}
module.exports = checkStraightFlush;
