const transformCards = require('./transformCards');
function transformflush(cards) {
    const newCards = transformCards(cards, 'suit');
    return newCards[0].count >= 5 ? cards.filter(card => card.suit === newCards[0].suit) : [];
}
module.exports = transformflush;
