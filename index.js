const senComb = require('./utils/seniorCombination');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const DECK = require('./cardDeck');
const app = express();

app.use(bodyParser.json());
app.use(cors());

app.get('/', function (req, res) {
  res.json(DECK);
});
app.post('/', function (req, res) {
  const result = senComb(req.body);
  res.json(result);
});

app.listen(3001, function () {
  console.log('Example app listening on port 3001!');
});
